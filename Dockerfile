FROM alpine:3.13.5 as list
ADD app /app
WORKDIR /app
RUN apk add --update --no-cache python3 ffmpeg && ln -sf python3 /usr/bin/python
RUN python3 -m ensurepip
RUN pip3 install --no-cache --upgrade pip setuptools && pip3 install -r requirements.txt
ENTRYPOINT [ "python3", "converter.py" ]
CMD ["list", "create", "--path", "/app/videos"]

FROM alpine:3.13.5 as converter
ADD app /app
WORKDIR /app
RUN apk add --update --no-cache python3 ffmpeg && ln -sf python3 /usr/bin/python
RUN python3 -m ensurepip
RUN pip3 install --no-cache --upgrade pip setuptools && pip3 install -r requirements.txt
ENTRYPOINT [ "python3", "converter.py" ]
CMD ["convert"]
